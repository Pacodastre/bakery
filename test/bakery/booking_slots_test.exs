defmodule Bakery.BookingSlotsTest do
  use Bakery.DataCase

  alias Bakery.BookingSlots

  describe "booking_slots" do
    alias Bakery.BookingSlots.BookingSlot

    @valid_attrs %{date: ~D[2010-04-17], time_slot: ~T[14:00:00]}
    @update_attrs %{date: ~D[2011-05-18], time_slot: ~T[15:01:01]}
    @invalid_attrs %{date: nil, time_slot: nil}

    def booking_slot_fixture(attrs \\ %{}) do
      {:ok, booking_slot} =
        attrs
        |> Enum.into(@valid_attrs)
        |> BookingSlots.create_booking_slot()

      booking_slot
    end

    test "list_booking_slots/0 returns all booking_slots" do
      booking_slot = booking_slot_fixture()
      assert BookingSlots.list_booking_slots() == [booking_slot]
    end

    test "get_booking_slot!/1 returns the booking_slot with given id" do
      booking_slot = booking_slot_fixture()
      assert BookingSlots.get_booking_slot!(booking_slot.id) == booking_slot
    end

    test "create_booking_slot/1 with valid data creates a booking_slot" do
      assert {:ok, %BookingSlot{} = booking_slot} = BookingSlots.create_booking_slot(@valid_attrs)
      assert booking_slot.date == ~D[2010-04-17]
      assert booking_slot.time_slot == ~T[14:00:00]
    end

    test "create_booking_slot/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = BookingSlots.create_booking_slot(@invalid_attrs)
    end

    test "update_booking_slot/2 with valid data updates the booking_slot" do
      booking_slot = booking_slot_fixture()
      assert {:ok, %BookingSlot{} = booking_slot} = BookingSlots.update_booking_slot(booking_slot, @update_attrs)
      assert booking_slot.date == ~D[2011-05-18]
      assert booking_slot.time_slot == ~T[15:01:01]
    end

    test "update_booking_slot/2 with invalid data returns error changeset" do
      booking_slot = booking_slot_fixture()
      assert {:error, %Ecto.Changeset{}} = BookingSlots.update_booking_slot(booking_slot, @invalid_attrs)
      assert booking_slot == BookingSlots.get_booking_slot!(booking_slot.id)
    end

    test "delete_booking_slot/1 deletes the booking_slot" do
      booking_slot = booking_slot_fixture()
      assert {:ok, %BookingSlot{}} = BookingSlots.delete_booking_slot(booking_slot)
      assert_raise Ecto.NoResultsError, fn -> BookingSlots.get_booking_slot!(booking_slot.id) end
    end

    test "change_booking_slot/1 returns a booking_slot changeset" do
      booking_slot = booking_slot_fixture()
      assert %Ecto.Changeset{} = BookingSlots.change_booking_slot(booking_slot)
    end
  end
end
