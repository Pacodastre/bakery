defmodule Bakery.SpecialOffersTest do
  use Bakery.DataCase

  alias Bakery.SpecialOffers

  describe "special_offers" do
    alias Bakery.SpecialOffers.SpecialOffer

    @valid_attrs %{disabled: true, name: "some name", order: 42, price: 120.5}
    @update_attrs %{disabled: false, name: "some updated name", order: 43, price: 456.7}
    @invalid_attrs %{disabled: nil, name: nil, order: nil, price: nil}

    def special_offer_fixture(attrs \\ %{}) do
      {:ok, special_offer} =
        attrs
        |> Enum.into(@valid_attrs)
        |> SpecialOffers.create_special_offer()

      special_offer
    end

    test "list_special_offers/0 returns all special_offers" do
      special_offer = special_offer_fixture()
      assert SpecialOffers.list_special_offers() == [special_offer]
    end

    test "get_special_offer!/1 returns the special_offer with given id" do
      special_offer = special_offer_fixture()
      assert SpecialOffers.get_special_offer!(special_offer.id) == special_offer
    end

    test "create_special_offer/1 with valid data creates a special_offer" do
      assert {:ok, %SpecialOffer{} = special_offer} = SpecialOffers.create_special_offer(@valid_attrs)
      assert special_offer.disabled == true
      assert special_offer.name == "some name"
      assert special_offer.order == 42
      assert special_offer.price == 120.5
    end

    test "create_special_offer/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = SpecialOffers.create_special_offer(@invalid_attrs)
    end

    test "update_special_offer/2 with valid data updates the special_offer" do
      special_offer = special_offer_fixture()
      assert {:ok, %SpecialOffer{} = special_offer} = SpecialOffers.update_special_offer(special_offer, @update_attrs)
      assert special_offer.disabled == false
      assert special_offer.name == "some updated name"
      assert special_offer.order == 43
      assert special_offer.price == 456.7
    end

    test "update_special_offer/2 with invalid data returns error changeset" do
      special_offer = special_offer_fixture()
      assert {:error, %Ecto.Changeset{}} = SpecialOffers.update_special_offer(special_offer, @invalid_attrs)
      assert special_offer == SpecialOffers.get_special_offer!(special_offer.id)
    end

    test "delete_special_offer/1 deletes the special_offer" do
      special_offer = special_offer_fixture()
      assert {:ok, %SpecialOffer{}} = SpecialOffers.delete_special_offer(special_offer)
      assert_raise Ecto.NoResultsError, fn -> SpecialOffers.get_special_offer!(special_offer.id) end
    end

    test "change_special_offer/1 returns a special_offer changeset" do
      special_offer = special_offer_fixture()
      assert %Ecto.Changeset{} = SpecialOffers.change_special_offer(special_offer)
    end
  end
end
