defmodule BakeryWeb.DeliveryControllerTest do
  use BakeryWeb.ConnCase

  alias Bakery.Deliveries
  alias Bakery.Deliveries.Delivery

  @create_attrs %{
    address1: "some address1",
    address2: "some address2",
    city: "some city",
    county: "some county",
    delivery_date: "2010-04-17T14:00:00Z",
    email: "some email",
    first_name: "some first_name",
    last_name: "some last_name",
    message: "some message",
    phone: "some phone"
  }
  @update_attrs %{
    address1: "some updated address1",
    address2: "some updated address2",
    city: "some updated city",
    county: "some updated county",
    delivery_date: "2011-05-18T15:01:01Z",
    email: "some updated email",
    first_name: "some updated first_name",
    last_name: "some updated last_name",
    message: "some updated message",
    phone: "some updated phone"
  }
  @invalid_attrs %{address1: nil, address2: nil, city: nil, county: nil, delivery_date: nil, email: nil, first_name: nil, last_name: nil, message: nil, phone: nil}

  def fixture(:delivery) do
    {:ok, delivery} = Deliveries.create_delivery(@create_attrs)
    delivery
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all deliveries", %{conn: conn} do
      conn = get(conn, Routes.delivery_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create delivery" do
    test "renders delivery when data is valid", %{conn: conn} do
      conn = post(conn, Routes.delivery_path(conn, :create), delivery: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.delivery_path(conn, :show, id))

      assert %{
               "id" => id,
               "address1" => "some address1",
               "address2" => "some address2",
               "city" => "some city",
               "county" => "some county",
               "delivery_date" => "2010-04-17T14:00:00Z",
               "email" => "some email",
               "first_name" => "some first_name",
               "last_name" => "some last_name",
               "message" => "some message",
               "phone" => "some phone"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.delivery_path(conn, :create), delivery: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update delivery" do
    setup [:create_delivery]

    test "renders delivery when data is valid", %{conn: conn, delivery: %Delivery{id: id} = delivery} do
      conn = put(conn, Routes.delivery_path(conn, :update, delivery), delivery: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.delivery_path(conn, :show, id))

      assert %{
               "id" => id,
               "address1" => "some updated address1",
               "address2" => "some updated address2",
               "city" => "some updated city",
               "county" => "some updated county",
               "delivery_date" => "2011-05-18T15:01:01Z",
               "email" => "some updated email",
               "first_name" => "some updated first_name",
               "last_name" => "some updated last_name",
               "message" => "some updated message",
               "phone" => "some updated phone"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, delivery: delivery} do
      conn = put(conn, Routes.delivery_path(conn, :update, delivery), delivery: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete delivery" do
    setup [:create_delivery]

    test "deletes chosen delivery", %{conn: conn, delivery: delivery} do
      conn = delete(conn, Routes.delivery_path(conn, :delete, delivery))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.delivery_path(conn, :show, delivery))
      end
    end
  end

  defp create_delivery(_) do
    delivery = fixture(:delivery)
    %{delivery: delivery}
  end
end
