defmodule BakeryWeb.SpecialOfferControllerTest do
  use BakeryWeb.ConnCase

  alias Bakery.SpecialOffers
  alias Bakery.SpecialOffers.SpecialOffer

  @create_attrs %{
    disabled: true,
    name: "some name",
    order: 42,
    price: 120.5
  }
  @update_attrs %{
    disabled: false,
    name: "some updated name",
    order: 43,
    price: 456.7
  }
  @invalid_attrs %{disabled: nil, name: nil, order: nil, price: nil}

  def fixture(:special_offer) do
    {:ok, special_offer} = SpecialOffers.create_special_offer(@create_attrs)
    special_offer
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all special_offers", %{conn: conn} do
      conn = get(conn, Routes.special_offer_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create special_offer" do
    test "renders special_offer when data is valid", %{conn: conn} do
      conn = post(conn, Routes.special_offer_path(conn, :create), special_offer: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.special_offer_path(conn, :show, id))

      assert %{
               "id" => id,
               "disabled" => true,
               "name" => "some name",
               "order" => 42,
               "price" => 120.5
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.special_offer_path(conn, :create), special_offer: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update special_offer" do
    setup [:create_special_offer]

    test "renders special_offer when data is valid", %{conn: conn, special_offer: %SpecialOffer{id: id} = special_offer} do
      conn = put(conn, Routes.special_offer_path(conn, :update, special_offer), special_offer: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.special_offer_path(conn, :show, id))

      assert %{
               "id" => id,
               "disabled" => false,
               "name" => "some updated name",
               "order" => 43,
               "price" => 456.7
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, special_offer: special_offer} do
      conn = put(conn, Routes.special_offer_path(conn, :update, special_offer), special_offer: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete special_offer" do
    setup [:create_special_offer]

    test "deletes chosen special_offer", %{conn: conn, special_offer: special_offer} do
      conn = delete(conn, Routes.special_offer_path(conn, :delete, special_offer))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.special_offer_path(conn, :show, special_offer))
      end
    end
  end

  defp create_special_offer(_) do
    special_offer = fixture(:special_offer)
    %{special_offer: special_offer}
  end
end
