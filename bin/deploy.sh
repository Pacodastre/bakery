ssh $1 /bin/bash << EOF
cd bakery;
source env.sh;

# Kill elixir process
pid=\`netstat -tlnp 2> /dev/null | grep :$2 | awk '{split(\$NF,a,"/"); print a[1]}'\`
echo "PID"
echo \$pid
if [ -n "\${pid-}" ]; then echo "killing elixir"; kill -9 \$pid; fi

# update code
git pull origin master

# update database
elixir --erl "-detached" -S mix phx.server
mix ecto.migrate
echo "Starting server"
exit
EOF
