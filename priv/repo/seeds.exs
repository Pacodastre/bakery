# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Bakery.Repo.insert!(%Bakery.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Bakery.Repo
alias Bakery.OrderStatuses.OrderStatus
alias Bakery.Settings.Setting

order_status_names = OrderStatus
|> Repo.all()
|> Enum.map(fn order_status -> order_status.status end)

order_statuses = [
  %{status: "waiting"},
  %{status: "confirmed"},
  %{status: "approved"},
  %{status: "declined"},
  %{status: "delivered"},
]

for order_status <- order_statuses do
  # If it's already in the database, we ignore it
  if not Enum.member?(order_status_names, order_status.status) do
    %OrderStatus{}
    |> OrderStatus.changeset(order_status)
    |> Repo.insert!()
  end
end


setting_names = Setting
|> Repo.all()
|> Enum.map(fn setting -> setting.name end)

settings = [
  %{name: "delivery_price", value: "3", type: "float"},
  %{name: "baker_email", value: "", type: "string"},
  %{name: "delivery_minimum_order", value: "15", type: "float"},
  %{name: "collection_minimum_order", value: "0", type: "float"},
  %{name: "instagram_url", value: "", type: "string"},
  %{name: "frontend_url", value: "http://localhost:3000/", type: "string"},
  %{name: "app_logo", value: "", type: "string"},
  %{name: "frontend_favicon", value: "", type: "string"},
  %{name: "admin_favicon", value: "", type: "string"},
  %{name: "about", value: "", type: "string"},
  %{name: "bakery_name", value: "Bakery", type: "string"},
]

for setting <- settings do
    if not Enum.member?(setting_names, setting.name) do
      %Setting{}
      |> Setting.changeset(setting)
      |> Repo.insert!()
    end
end
