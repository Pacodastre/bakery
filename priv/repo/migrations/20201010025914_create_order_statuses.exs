defmodule Bakery.Repo.Migrations.CreateOrderStatuses do
  use Ecto.Migration

  def change do
    create table(:order_statuses) do
      add :status, :text

      timestamps()
    end

  end
end
