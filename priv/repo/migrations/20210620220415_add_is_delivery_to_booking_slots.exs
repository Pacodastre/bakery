defmodule Bakery.Repo.Migrations.AddIsDeliveryToBookingSlots do
  use Ecto.Migration

  def change do
    alter table(:booking_slots) do
      add :is_delivery, :boolean, default: :true
    end
  end
end
