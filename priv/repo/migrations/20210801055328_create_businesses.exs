defmodule Bakery.Repo.Migrations.CreateBusinesses do
  use Ecto.Migration

  def change do
    create table(:businesses) do
      add :name, :text
      add :address1, :text
      add :address2, :text
      add :city, :text
      add :county, :text
      add :phone, :text

      timestamps()
    end

  end
end
