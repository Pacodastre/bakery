defmodule Bakery.Repo.Migrations.AddIsDeliveryToOrders do
  use Ecto.Migration

  def change do
    alter table(:orders) do
      add :is_delivery, :boolean, default: :true
    end
  end
end
