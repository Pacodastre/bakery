defmodule Bakery.Repo.Migrations.CreateProductsMayContainAllergensTable do
  use Ecto.Migration

  def change do
    create table(:products_may_contain_allergens) do
      add :product_id, references(:products, on_delete: :nothing)
      add :allergen_id, references(:allergens, on_delete: :nothing)
    end
    create index(:products_may_contain_allergens, [:product_id])
    create index(:products_may_contain_allergens, [:allergen_id])
  end
end
