defmodule Bakery.Repo.Migrations.CreateDeliveries do
  use Ecto.Migration

  def change do
    create table(:deliveries) do
      add :delivery_date, :utc_datetime
      add :first_name, :text
      add :last_name, :text
      add :email, :text
      add :phone, :text
      add :address1, :text
      add :address2, :text
      add :city, :text
      add :county, :text
      add :message, :text

      timestamps()
    end
  end
end
