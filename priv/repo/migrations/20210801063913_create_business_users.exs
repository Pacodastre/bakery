defmodule Bakery.Repo.Migrations.CreateBusinessUsers do
  use Ecto.Migration

  def change do
    create table(:business_users) do
      add :email, :text
      add :hashed_password, :text
      add :business_id, references(:businesses, on_delete: :nothing)

      timestamps()
    end

    create index(:business_users, [:business_id])
    create unique_index(:business_users, [:email])
  end
end
