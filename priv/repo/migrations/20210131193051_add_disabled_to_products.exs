defmodule Bakery.Repo.Migrations.AddDisabledToProducts do
  use Ecto.Migration

  alias Bakery.Products.Product

  def change do
    alter table(:products) do
      add :order, :integer
      add :disabled, :boolean, default: :true
    end

    flush()

    Product
    |> Bakery.Repo.all()
    |> Enum.each(fn product ->
      Bakery.Repo.update!(
        Product.changeset(product, %{disabled: false})
      ) end)
  end
end
