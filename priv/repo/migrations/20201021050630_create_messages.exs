defmodule Bakery.Repo.Migrations.CreateMessages do
  use Ecto.Migration

  def change do
    create table(:messages) do
      add :name, :text
      add :email, :text
      add :message, :text

      timestamps()
    end

  end
end
