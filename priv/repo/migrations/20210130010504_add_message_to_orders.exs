defmodule Bakery.Repo.Migrations.AddMessageToOrders do
  use Ecto.Migration

  def change do
    alter table(:orders) do
      add :message, :text
    end
  end
end
