defmodule Bakery.Repo.Migrations.CreatePayments do
  use Ecto.Migration

  def change do
    create table(:payments) do
      add :type, :string
      add :transaction_code, :string

      timestamps()
    end

  end
end
