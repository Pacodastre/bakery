defmodule Bakery.Repo.Migrations.AddPaymentIdToOrders do
  use Ecto.Migration

  def change do
    alter table(:orders) do
      add :payment_id, references(:payments, on_delete: :nothing)
    end
  end
end
