defmodule Bakery.Repo.Migrations.AddSpecialOfferIdToTrolleyItems do
  use Ecto.Migration

  def change do
    alter table(:trolley_items) do
      add :special_offer_id, references(:special_offers, on_delete: :nothing)
    end
  end
end
