defmodule Bakery.Repo.Migrations.CreateTrolleyItems do
  use Ecto.Migration

  def change do
    create table(:trolley_items) do
      add :quantity, :integer
      add :unit_price, :float
      add :trolley_id, references(:trolleys, on_delete: :nothing)
      add :product_id, references(:products, on_delete: :nothing)

      timestamps()
    end

    create index(:trolley_items, [:trolley_id])
    create index(:trolley_items, [:product_id])
  end
end
