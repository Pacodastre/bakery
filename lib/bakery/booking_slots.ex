defmodule Bakery.BookingSlots do
  @moduledoc """
  The BookingSlots context.
  """

  import Ecto.Query, warn: false
  alias Bakery.Repo

  alias Bakery.BookingSlots.BookingSlot

  @doc """
  Returns the list of booking_slots.

  ## Examples

      iex> list_booking_slots()
      [%BookingSlot{}, ...]

  """
  def list_booking_slots(date_string) do
    {:ok, date} = date_string
    |> Date.from_iso8601()

    monday = date
    |> Date.beginning_of_week()

    next_monday = Date.add(monday, 7)
    two_days_from_now = Date.add(Date.utc_today, 2)

    BookingSlot
    |> where(
      [booking_slot],
        ^monday <= booking_slot.date
        and ^next_monday > booking_slot.date
        and ^two_days_from_now <= booking_slot.date
        and is_nil(booking_slot.order_id)
    )
    |> order_by([:date, :time_slot])
    |> Repo.all
  end

  def admin_list_booking_slots(date_string) do
    {:ok, date} = date_string
    |> Date.from_iso8601()

    monday = date
    |> Date.beginning_of_week()
    next_monday = Date.add(monday, 7)

    BookingSlot
    |> where(
      [booking_slot],
        ^monday <= booking_slot.date
        and ^next_monday > booking_slot.date
    )
    |> order_by([:date, :time_slot])
    |> Repo.all
  end

  @doc """
  Gets a single booking_slot.

  Raises `Ecto.NoResultsError` if the Booking slot does not exist.

  ## Examples

      iex> get_booking_slot!(123)
      %BookingSlot{}

      iex> get_booking_slot!(456)
      ** (Ecto.NoResultsError)

  """
  def get_booking_slot!(id), do: Repo.get!(BookingSlot, id)

  def get_earliest_booking_slot() do
    two_days_from_now = Date.add(Date.utc_today(), 2)
    BookingSlot
    |> where(
      [booking_slot],
        booking_slot.date>= ^two_days_from_now
        and is_nil(booking_slot.order_id)
    )
    |> first([:date, :time_slot])
    |> Repo.one
  end

  def find_booking_slot(date) do
    BookingSlot
    |> where([booking_slot], booking_slot.date == ^date and is_nil(booking_slot.order_id))
    |> first(:date)
    |> Repo.one
  end


  @doc """
  Creates a booking_slot.

  ## Examples

      iex> create_booking_slot(%{field: value})
      {:ok, %BookingSlot{}}

      iex> create_booking_slot(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_booking_slot(attrs \\ %{}) do
    %BookingSlot{}
    |> BookingSlot.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a booking_slot.

  ## Examples

      iex> update_booking_slot(booking_slot, %{field: new_value})
      {:ok, %BookingSlot{}}

      iex> update_booking_slot(booking_slot, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_booking_slot(%BookingSlot{} = booking_slot, attrs) do
    booking_slot
    |> BookingSlot.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a booking_slot.

  ## Examples

      iex> delete_booking_slot(booking_slot)
      {:ok, %BookingSlot{}}

      iex> delete_booking_slot(booking_slot)
      {:error, %Ecto.Changeset{}}

  """
  def delete_booking_slot(%BookingSlot{} = booking_slot) do
    Repo.delete(booking_slot)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking booking_slot changes.

  ## Examples

      iex> change_booking_slot(booking_slot)
      %Ecto.Changeset{data: %BookingSlot{}}

  """
  def change_booking_slot(%BookingSlot{} = booking_slot, attrs \\ %{}) do
    BookingSlot.changeset(booking_slot, attrs)
  end
end
