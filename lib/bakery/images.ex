defmodule Bakery.Images do
  @moduledoc """
  The Images context.
  """

  import Ecto.Query, warn: false
  alias Bakery.Repo

  alias Bakery.Images.Image

  @doc """
  Returns the list of images.

  ## Examples

      iex> list_images()
      [%Image{}, ...]

  """
  def list_images do
    Repo.all(Image)
  end

  @doc """
  Gets a single image.

  Raises `Ecto.NoResultsError` if the Image does not exist.

  ## Examples

      iex> get_image!(123)
      %Image{}

      iex> get_image!(456)
      ** (Ecto.NoResultsError)

  """
  def get_image!(id), do: Repo.get!(Image, id)

  def get_image_by!(params) do
    Repo.get_by!(Image, params)
  end

  def get_file_type(content_type) do
    [_content_type, extension] = String.split(content_type, "/")
    if String.contains?(extension, "+") do
      [file_type, _end] = String.split(extension, "+")
      file_type
    else
      extension
    end
  end

  @doc """
  Creates a image.

  ## Examples

      iex> create_image(%{field: value})
      {:ok, %Image{}}

      iex> create_image(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_image(%Plug.Upload{path: path, content_type: content_type} \\ %{}) do
    file_type = get_file_type(content_type)
    filename = "#{UUID.uuid4()}.#{file_type}"
    File.cp!(path, "#{Application.get_env(:bakery, :upload_path)}/#{filename}")
    %Image{}
    |> Image.changeset(%{filename: filename})
    |> Repo.insert()
  end

  @doc """
  Updates a image.

  ## Examples

      iex> update_image(image, %{field: new_value})
      {:ok, %Image{}}

      iex> update_image(image, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_image(%Image{} = image, attrs) do
    image
    |> Image.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a image.

  ## Examples

      iex> delete_image(image)
      {:ok, %Image{}}

      iex> delete_image(image)
      {:error, %Ecto.Changeset{}}

  """
  def delete_image(%Image{} = image) do
    output = Repo.delete(image)
    File.rm("#{Application.get_env(:bakery, :upload_path)}/#{image.filename}")
    output
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking image changes.

  ## Examples

      iex> change_image(image)
      %Ecto.Changeset{data: %Image{}}

  """
  def change_image(%Image{} = image, attrs \\ %{}) do
    Image.changeset(image, attrs)
  end
end
