defmodule Bakery.Orders do
  @moduledoc """
  The Orders context.
  """

  import Ecto.Query, warn: false
  alias Bakery.Repo

  alias Bakery.Orders.Order
  alias Bakery.OrderStatuses
  alias Bakery.ConfirmationKeys
  alias Bakery.ConfirmationKeys.ConfirmationKey

  @doc """
  Returns the list of orders.

  ## Examples

      iex> list_orders()
      [%Order{}, ...]

  """
  def list_orders do
    query = from order in Order,
      join: delivery in assoc(order, :delivery),
      order_by: [delivery.delivery_date, delivery.time_slot]
    query
      |> Repo.all()
      |> Repo.preload([:delivery, :payment, :status, :customer, trolley: [:items]])
  end

  def list_orders(preload \\ [:delivery, :payment, :status, :customer, trolley: [:items]])  do
    query = from order in Order,
      join: delivery in assoc(order, :delivery),
      order_by: [delivery.delivery_date, delivery.time_slot]
    query
    |> Repo.all()
    |> Repo.preload(preload)
  end

  @doc """
  Gets a single order.

  Raises `Ecto.NoResultsError` if the Order does not exist.

  ## Examples

      iex> get_order!(123)
      %Order{}

      iex> get_order!(456)
      ** (Ecto.NoResultsError)

  """
  def get_order!(id) do
    Repo.get!(Order, id)
    |> Repo.preload([:delivery, :payment, :status, :customer, trolley: [:items]])
  end

  def get_order_with_confirmation_key!(id, confirmation_key) do
    ConfirmationKey
    |> Repo.get_by!([confirmation_key: confirmation_key])
    |> Ecto.assoc(:order)
    |> Repo.get!(id)
    |> Repo.preload([:delivery, :payment, :status, :customer, trolley: [:items]])
  end

  @doc """
  Creates a order.

  ## Examples

      iex> create_order(%{field: value})
      {:ok, %Order{}}

      iex> create_order(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order(%{"is_delivery" => true} = attrs) do
    %Order{}
    |> Repo.preload([:delivery, :payment, :trolley, :customer, :status])
    |> Order.create_delivery_changeset(attrs)
    |> Ecto.Changeset.put_change(:status, OrderStatuses.get_order_status_by_status!("waiting"))
    |> Repo.insert()
  end

  def create_order(%{"is_delivery" => false} = attrs) do
    %Order{}
    |> Repo.preload([:delivery, :payment, :trolley, :customer, :status])
    |> Order.create_collection_changeset(attrs)
    |> Ecto.Changeset.put_change(:status, OrderStatuses.get_order_status_by_status!("waiting"))
    |> Repo.insert()
    end

  @doc """
  Updates a order.

  ## Examples

      iex> update_order(order, %{field: new_value})
      {:ok, %Order{}}

      iex> update_order(order, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_order(%Order{} = order, attrs) do
    order
    |> Order.changeset(attrs)
    |> Repo.update()
  end

  def admin_update_order(%Order{} = order, attrs) do
    order
    |> Order.admin_changeset(attrs)
    |> Repo.update()
  end

  def confirm_order(id, confirmation_key) do
    if ConfirmationKeys.confirmation_key_valid?(confirmation_key, id) do
      id
      |> get_order!()
      |> change_order()
      |> Ecto.Changeset.put_change(:status_id, OrderStatuses.get_order_status_by_status!("confirmed").id)
      |> Repo.update()
    end
  end

  def approve_order(%Order{} = order, message) do
    order
    |> admin_change_order()
    |> Ecto.Changeset.put_change(:status_id, OrderStatuses.get_order_status_by_status!("approved").id)
    |> Ecto.Changeset.put_change(:message, message)
    |> Repo.update()
  end

  def decline_order(%Order{} = order, message) do
    order
    |> Repo.preload([:payment, :customer, trolley: [items: [:product, [special_offer: :products]]]])
    |> admin_change_order()
    |> Ecto.Changeset.put_change(:status_id, OrderStatuses.get_order_status_by_status!("declined").id)
    |> Ecto.Changeset.put_change(:message, message)
    |> Repo.update()
  end

  @doc """
  Deletes a order.

  ## Examples

      iex> delete_order(order)
      {:ok, %Order{}}

      iex> delete_order(order)
      {:error, %Ecto.Changeset{}}

  """
  def delete_order(%Order{} = order) do
    Repo.delete(order)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking order changes.

  ## Examples

      iex> change_order(order)
      %Ecto.Changeset{data: %Order{}}

  """
  def change_order(%Order{} = order, attrs \\ %{}) do
    Order.changeset(order, attrs)
  end

  def admin_change_order(%Order{} = order, attrs \\ %{}) do
    Order.admin_changeset(order, attrs)
  end
end
