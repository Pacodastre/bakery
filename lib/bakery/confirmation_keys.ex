defmodule Bakery.ConfirmationKeys do
  @moduledoc """
  The ConfirmationKeys context.
  """

  import Ecto.Query, warn: false
  alias Bakery.Repo

  alias Bakery.ConfirmationKeys.ConfirmationKey
  alias Bakery.RandomString

  @doc """
  Returns the list of confirmation_keys.

  ## Examples

      iex> list_confirmation_keys()
      [%ConfirmationKey{}, ...]

  """
  def list_confirmation_keys do
    Repo.all(ConfirmationKey)
  end

  @doc """
  Gets a single confirmation_key.

  Raises `Ecto.NoResultsError` if the Confirmation key does not exist.

  ## Examples

      iex> get_confirmation_key!(123)
      %ConfirmationKey{}

      iex> get_confirmation_key!(456)
      ** (Ecto.NoResultsError)

  """
  def get_confirmation_key!(id), do: Repo.get!(ConfirmationKey, id)

  def confirmation_key_valid?(confirmation_key, order_id) do
    results = from(
      confirmation_key in ConfirmationKey,
      where: (
        confirmation_key.order_id == ^order_id
        and confirmation_key.confirmation_key == ^confirmation_key
        and confirmation_key.expires_on >= ^DateTime.utc_now()
      ))
    |> Repo.all()

    length(results) > 0
  end

  @doc """
  Creates a confirmation_key.

  ## Examples

      iex> create_confirmation_key(%{field: value})
      {:ok, %ConfirmationKey{}}

      iex> create_confirmation_key(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_confirmation_key(attrs \\ %{}) do
    full_attrs = attrs
    |> Map.put(:confirmation_key, RandomString.random_string())
    |> Map.put(:expires_on, DateTime.add(DateTime.utc_now, 14 * 60 * 60 * 24, :second))

    %ConfirmationKey{}
    |> ConfirmationKey.changeset(full_attrs)
    |> Repo.insert!()
  end

  @doc """
  Updates a confirmation_key.

  ## Examples

      iex> update_confirmation_key(confirmation_key, %{field: new_value})
      {:ok, %ConfirmationKey{}}

      iex> update_confirmation_key(confirmation_key, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_confirmation_key(%ConfirmationKey{} = confirmation_key, attrs) do
    confirmation_key
    |> ConfirmationKey.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a confirmation_key.

  ## Examples

      iex> delete_confirmation_key(confirmation_key)
      {:ok, %ConfirmationKey{}}

      iex> delete_confirmation_key(confirmation_key)
      {:error, %Ecto.Changeset{}}

  """
  def delete_confirmation_key(%ConfirmationKey{} = confirmation_key) do
    Repo.delete(confirmation_key)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking confirmation_key changes.

  ## Examples

      iex> change_confirmation_key(confirmation_key)
      %Ecto.Changeset{data: %ConfirmationKey{}}

  """
  def change_confirmation_key(%ConfirmationKey{} = confirmation_key, attrs \\ %{}) do
    ConfirmationKey.changeset(confirmation_key, attrs)
  end
end
