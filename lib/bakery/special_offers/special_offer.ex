defmodule Bakery.SpecialOffers.SpecialOffer do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bakery.Products

  schema "special_offers" do
    field :disabled, :boolean, default: true
    field :name, :string
    field :order, :integer
    field :price, :float
    field :image, :string
    many_to_many :products, Products.Product,
      join_through: "special_offers_products",
      on_delete: :delete_all,
      on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(special_offer, attrs) do
    special_offer
    |> cast(attrs, [:name, :order, :disabled, :price, :disabled, :image])
    |> validate_required([:name, :order, :disabled, :price, :disabled, :image])
  end
end
