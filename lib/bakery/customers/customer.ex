defmodule Bakery.Customers.Customer do
  use Ecto.Schema
  import Ecto.Changeset

  schema "customers" do
    field :address1, :string
    field :address2, :string
    field :city, :string
    field :county, :string
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    field :phone, :string

    timestamps()
  end

  @doc false
  def changeset(customer, attrs) do
    customer
    |> cast(attrs, [:first_name, :last_name, :email, :phone, :address1, :address2, :city, :county])
    |> validate_required([:first_name, :last_name, :email, :phone, :address1, :address2, :city, :county])
  end

  @doc false
  def collection_changeset(customer, attrs) do
    customer
    |> cast(attrs, [:first_name, :last_name, :email, :phone])
    |> validate_required([:first_name, :last_name, :email, :phone])
  end

  @doc false
  def admin_changeset(customer, attrs) do
    customer
    |> cast(attrs, [:first_name, :last_name, :email, :phone, :address1, :address2, :city, :county])
    |> validate_required([:first_name, :last_name, :email, :phone])
  end
end
