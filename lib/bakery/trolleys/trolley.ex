defmodule Bakery.Trolleys.Trolley do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bakery.Trolleys.TrolleyItem

  schema "trolleys" do
    field :delivery_price, :float

    has_many :items, TrolleyItem

    timestamps()
  end

  @doc false
  def changeset(trolley, attrs) do
    trolley
    |> cast(attrs, [:delivery_price])
    |> cast_assoc(:items, with: &TrolleyItem.changeset/2)
    |> validate_required([:delivery_price, :items])
  end
end
