defmodule Bakery.Users do
  @moduledoc """
  The Users context.
  """

  import Ecto.Query, warn: false
  alias Bakery.Repo

  alias Bakery.Users.User

  def login(user_params) do
    user = Repo.get_by(User, email: user_params["email"])
    password = user_params["password"]
    case authenticate(user, password) do
      false ->
        {:error, "User not found"}
      _   ->
        {:ok, jwt, _full_claims} = user
        |> Bakery.Guardian.encode_and_sign(%{}, ttl: {1, :weeks})

        {:ok, jwt}
    end
  end

  def authenticate(user, password) do
    case user do
      nil ->
        false
      _ ->
        Bcrypt.verify_pass(password, user.hashed_password)
    end
  end

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Ecto.Changeset.put_change(:hashed_password, Bcrypt.hash_pwd_salt(attrs["password"]))
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end
end
