defmodule Bakery.BusinessUsers.BusinessUser do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bakery.Businesses.Business

  schema "business_users" do
    field :email, :string
    field :hashed_password, :string
    field :password, :string, virtual: true
    belongs_to :business, Business, on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(business_user, attrs) do
    business_user
    |> cast(attrs, [:email, :password, :business_id])
    |> validate_required([:email, :password, :business_id])
    |> unique_constraint(:email)
  end
end
