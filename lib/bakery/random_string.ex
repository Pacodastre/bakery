defmodule Bakery.RandomString do
  def random_string() do
    :crypto.strong_rand_bytes(32) |> Base.url_encode64 |> binary_part(0, 32)
  end
end
