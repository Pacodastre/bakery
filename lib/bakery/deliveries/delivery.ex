defmodule Bakery.Deliveries.Delivery do
  use Ecto.Schema
  import Ecto.Changeset

  schema "deliveries" do
    field :delivery_date, :date
    field :time_slot, :integer
    field :message, :string

    timestamps()
  end

  @doc false
  def changeset(delivery, attrs) do
    delivery
    |> cast(attrs, [:delivery_date, :time_slot, :message])
    |> validate_required([:delivery_date, :time_slot])
  end
end
