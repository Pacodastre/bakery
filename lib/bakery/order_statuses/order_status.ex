defmodule Bakery.OrderStatuses.OrderStatus do
  use Ecto.Schema
  import Ecto.Changeset

  schema "order_statuses" do
    field :status, :string

    timestamps()
  end

  @doc false
  def changeset(order_status, attrs) do
    order_status
    |> cast(attrs, [:status])
    |> validate_required([:status])
  end
end
