defmodule Bakery.Orders.Order do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bakery.OrderStatuses.OrderStatus
  alias Bakery.Deliveries.Delivery
  alias Bakery.Trolleys.Trolley
  alias Bakery.Customers.Customer
  alias Bakery.Payments.Payment

  @primary_key {:id, :binary_id, autogenerate: true}

  schema "orders" do
    belongs_to :status, OrderStatus
    belongs_to :delivery, Delivery, on_replace: :nilify
    belongs_to :trolley, Trolley, on_replace: :nilify
    belongs_to :customer, Customer, on_replace: :nilify
    belongs_to :payment, Payment, on_replace: :nilify
    field :message, :string
    field :is_delivery, :boolean

    timestamps()
  end

  @doc false
  def create_delivery_changeset(order, attrs) do
    order
    |> cast(attrs, [:status_id, :is_delivery])
    |> cast_assoc(:delivery, with: &Delivery.changeset/2)
    |> cast_assoc(:trolley, with: &Trolley.changeset/2)
    |> cast_assoc(:payment, with: &Payment.changeset/2)
    |> cast_assoc(:customer, with: &Customer.changeset/2)
    |> validate_required([:delivery, :trolley, :payment, :customer, :is_delivery])
  end

  @doc false
  def create_collection_changeset(order, attrs) do
    order
    |> cast(attrs, [:status_id, :is_delivery])
    |> cast_assoc(:delivery, with: &Delivery.changeset/2)
    |> cast_assoc(:trolley, with: &Trolley.changeset/2)
    |> cast_assoc(:payment, with: &Payment.changeset/2)
    |> cast_assoc(:customer, with: &Customer.collection_changeset/2)
    |> validate_required([:delivery, :trolley, :payment, :customer, :is_delivery])
  end

  @doc false
  def changeset(order, attrs) do
    order
    |> cast(attrs, [:status_id, :is_delivery])
    |> cast_assoc(:delivery, with: &Delivery.changeset/2)
    |> cast_assoc(:trolley, with: &Trolley.changeset/2)
    |> cast_assoc(:payment, with: &Payment.changeset/2)
    |> cast_assoc(:customer, with: &Customer.changeset/2)
    |> validate_required([:delivery, :trolley, :payment, :customer, :status_id, :is_delivery])
  end

  @doc false
  def admin_changeset(order, attrs) do
    order
    |> cast(attrs, [:status_id, :message, :is_delivery])
    |> cast_assoc(:delivery, with: &Delivery.changeset/2)
    |> cast_assoc(:trolley, with: &Trolley.changeset/2)
    |> cast_assoc(:payment, with: &Payment.admin_changeset/2)
    |> cast_assoc(:customer, with: &Customer.admin_changeset/2)
    |> validate_required([:delivery, :trolley, :payment, :customer, :status_id, :is_delivery])
  end
end
