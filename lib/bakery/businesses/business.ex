defmodule Bakery.Businesses.Business do
  use Ecto.Schema
  import Ecto.Changeset

  schema "businesses" do
    field :name, :string
    field :address1, :string
    field :address2, :string
    field :city, :string
    field :county, :string
    field :phone, :string

    timestamps()
  end

  @doc false
  def changeset(business, attrs) do
    business
    |> cast(attrs, [:name, :address1, :address2, :city, :county, :phone])
    |> validate_required([:name, :address1, :address2, :city, :county, :phone])
  end
end
