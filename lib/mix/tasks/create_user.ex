defmodule Mix.Tasks.CreateUser do
  use Mix.Task

  alias Bakery.Users

  @shortdoc "Creates a user"
  def run(_) do
    Mix.Task.run("app.start")

    email = IO.gets("Choose an email for your new user:\n")
    |> String.trim("\n")
    password = IO.gets("Choose a password:\n")
    |> String.trim("\n")

    IO.inspect Users.create_user(%{"email" => email, "password" => password})
  end
end
