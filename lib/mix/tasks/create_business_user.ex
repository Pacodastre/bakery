defmodule Mix.Tasks.CreateBusinessUser do
  use Mix.Task

  alias Bakery.BusinessUsers
  alias Bakery.Businesses

  @shortdoc "Creates a business"
  def run(_) do
    Mix.Task.run("app.start")

    email = IO.gets("Choose an email for your new business user:\n")
    |> String.trim("\n")
    password = IO.gets("Choose a password:\n")
    |> String.trim("\n")
    businesses = Businesses.list_businesses()
    IO.puts("List of businesses:")
    Enum.each(businesses, fn business -> IO.puts("#{business.id}: #{business.name}") end)
    business_id = IO.gets("Choose a business:\n")
    |> String.trim("\n")


    IO.inspect BusinessUsers.create_business_user(%{
      "email" => email,
      "password" => password,
      "business_id" => business_id,
    })
  end
end
