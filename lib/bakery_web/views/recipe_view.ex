defmodule BakeryWeb.RecipeView do
  use BakeryWeb, :view
  alias BakeryWeb.RecipeView

  def render("index.json", %{recipes: recipes}) do
    %{data: render_many(recipes, RecipeView, "recipe.json")}
  end

  def render("show.json", %{recipe: recipe}) do
    %{data: render_one(recipe, RecipeView, "recipe.json")}
  end

  def render("recipe.json", %{recipe: recipe}) do
    %{id: recipe.id,
      recipe: recipe.recipe,
      product_id: recipe.product_id}
  end
end
