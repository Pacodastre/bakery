defmodule BakeryWeb.SessionView do
  use BakeryWeb, :view

  def render("show.json", %{jwt: jwt}) do
    %{jwt: jwt}
  end

  def render("error.json", _) do
    %{error: "Invalid username or password"}
  end

  def render("unauthorized.json", _) do
    %{error: "Not an admin user"}
  end
end
