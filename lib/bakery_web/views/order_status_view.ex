defmodule BakeryWeb.OrderStatusView do
  use BakeryWeb, :view
  alias BakeryWeb.OrderStatusView

  def render("index.json", %{order_statuses: order_statuses}) do
    %{data: render_many(order_statuses, OrderStatusView, "order_status.json")}
  end

  def render("show.json", %{order_status: order_status}) do
    %{data: render_one(order_status, OrderStatusView, "order_status.json")}
  end

  def render("order_status.json", %{order_status: order_status}) do
    %{id: order_status.id,
      status: order_status.status}
  end
end
