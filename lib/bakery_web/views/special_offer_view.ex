defmodule BakeryWeb.SpecialOfferView do
  use BakeryWeb, :view
  alias BakeryWeb.SpecialOfferView

  def render("index.json", %{special_offers: special_offers}) do
    %{data: render_many(special_offers, SpecialOfferView, "special_offer.json")}
  end

  def render("show.json", %{special_offer: special_offer}) do
    %{data: render_one(special_offer, SpecialOfferView, "special_offer.json")}
  end

  def render("special_offer.json", %{special_offer: special_offer}) do
    %{id: special_offer.id,
      name: special_offer.name,
      order: special_offer.order,
      price: special_offer.price,
      disabled: special_offer.disabled,
      image: special_offer.image,
      products: Enum.map(special_offer.products, fn product -> product.id end),
    }
  end
end
