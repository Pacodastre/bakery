defmodule BakeryWeb.OrderView do
  use BakeryWeb, :view
  alias BakeryWeb.OrderView
  alias BakeryWeb.DeliveryView
  alias BakeryWeb.TrolleyView
  alias BakeryWeb.PaymentView
  alias BakeryWeb.CustomerView

  def render("index.json", %{orders: orders}) do
    %{data: render_many(orders, OrderView, "order.json")}
  end

  def render("show.json", %{order: order}) do
    %{data: render_one(order, OrderView, "order.json")}
  end

  def render("order.json", %{order: order}) do
    %{
      id: order.id,
      delivery: render_one(order.delivery, DeliveryView, "delivery.json"),
      trolley: render_one(order.trolley, TrolleyView, "trolley.json"),
      payment: render_one(order.payment, PaymentView, "payment.json"),
      customer: render_one(order.customer, CustomerView, "customer.json"),
      status_id: order.status_id,
      message: order.message,
      inserted_at: order.inserted_at,
      is_delivery: order.is_delivery,
    }
  end
end
