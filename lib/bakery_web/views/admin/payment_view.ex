defmodule BakeryWeb.AdminPaymentView do
  use BakeryWeb, :view

  def render("index.json", %{payments: payments}) do
    %{data: render_many(payments, AdminPaymentView, "payment.json")}
  end

  def render("show.json", %{payment: payment}) do
    %{data: render_one(payment, AdminPaymentView, "payment.json")}
  end

  def render("payment.json", %{admin_payment: payment}) do
    %{id: payment.id,
      type: payment.type,
      transaction_code: payment.transaction_code,
    }
  end
end
