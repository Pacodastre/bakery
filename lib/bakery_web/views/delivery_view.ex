defmodule BakeryWeb.DeliveryView do
  use BakeryWeb, :view
  alias BakeryWeb.DeliveryView

  def render("index.json", %{deliveries: deliveries}) do
    %{data: render_many(deliveries, DeliveryView, "delivery.json")}
  end

  def render("show.json", %{delivery: delivery}) do
    %{data: render_one(delivery, DeliveryView, "delivery.json")}
  end

  def render("delivery.json", %{delivery: delivery}) do
    %{id: delivery.id,
      delivery_date: delivery.delivery_date,
      time_slot: delivery.time_slot,
      message: delivery.message}
  end
end
