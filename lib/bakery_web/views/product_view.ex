defmodule BakeryWeb.ProductView do
  use BakeryWeb, :view
  alias BakeryWeb.ProductView

  def render("index.json", %{products: products}) do
    %{data: render_many(products, ProductView, "product.json")}
  end

  def render("show.json", %{product: product}) do
    %{data: render_one(product, ProductView, "product.json")}
  end

  def render("product.json", %{product: product}) do
    %{id: product.id,
      name: product.name,
      key: product.key,
      description: product.description,
      image: product.image,
      price: product.price,
      disabled: product.disabled,
      order: product.order,
      allergens: Enum.map(product.allergens, fn allergen -> allergen.id end),
      may_contain_allergens: Enum.map(product.may_contain_allergens, fn allergen -> allergen.id end),
    }
  end
end
