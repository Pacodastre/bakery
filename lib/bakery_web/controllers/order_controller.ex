defmodule BakeryWeb.OrderController do
  use BakeryWeb, :controller

  alias Bakery.Orders
  alias Bakery.Orders.Order
  alias Bakery.Mailer
  alias Bakery.ConfirmationKeys
  alias Bakery.Settings

  action_fallback BakeryWeb.FallbackController

  def create(conn, %{"order" => order_params}) do
    with {:ok, %Order{} = order} <- Orders.create_order(order_params) do
      order = order
      |> Bakery.Repo.preload([:delivery, trolley: [items: [:product, special_offer: [:products]]]])

      confirmation_order = ConfirmationKeys.create_confirmation_key(%{order_id: order.id})
      Mailer.send_confirmation_email(order, confirmation_order)

      conn
      |> put_status(:created)
      |> render("show.json", order: order)
    end
  end

  def fetch_with_confirmation_key(conn, %{"id" => id, "confirmation_key" => confirmation_key}) do
    order = Orders.get_order_with_confirmation_key!(id, confirmation_key)
    render(conn, "show.json", order: order)
  end

  def confirm(conn, %{"id" => id, "confirmation_key" => confirmation_key}) do
    with {:ok, %Order{} = order} <- Orders.confirm_order(id, confirmation_key) do
      order = order
      |> Bakery.Repo.preload([:delivery, trolley: [items: [:product, special_offer: :products]]])

      baker_email = Settings.get_setting_by_name!("baker_email").value
      Mailer.send_confirmation_email_to_baker(order, baker_email)
      render(conn, "show.json", order: order)
    end
  end
end
