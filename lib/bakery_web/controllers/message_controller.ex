defmodule BakeryWeb.MessageController do
  use BakeryWeb, :controller

  alias Bakery.Messages
  alias Bakery.Messages.Message
  alias Bakery.Mailer
  alias Bakery.Settings

  action_fallback BakeryWeb.FallbackController

  def index(conn, _params) do
    messages = Messages.list_messages()
    render(conn, "index.json", messages: messages)
  end

  def create(conn, %{"message" => message_params}) do
    with {:ok, %Message{} = message} <- Messages.create_message(message_params) do
      baker_email = Settings.get_setting_by_name!("baker_email").value
      Mailer.send_message_received_email_to_baker(message, baker_email)
      conn
      |> put_status(:created)
      |> render("show.json", message: message)
    end
  end

  def show(conn, %{"id" => id}) do
    message = Messages.get_message!(id)
    render(conn, "show.json", message: message)
  end

  def update(conn, %{"id" => id, "message" => message_params}) do
    message = Messages.get_message!(id)

    with {:ok, %Message{} = message} <- Messages.update_message(message, message_params) do
      render(conn, "show.json", message: message)
    end
  end

  def delete(conn, %{"id" => id}) do
    message = Messages.get_message!(id)

    with {:ok, %Message{}} <- Messages.delete_message(message) do
      send_resp(conn, :no_content, "")
    end
  end
end
