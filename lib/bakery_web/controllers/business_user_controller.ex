defmodule BakeryWeb.BusinessUserController do
  use BakeryWeb, :controller

  alias Bakery.BusinessUsers
  alias Bakery.BusinessUsers.BusinessUser

  action_fallback BakeryWeb.FallbackController

  def index(conn, _params) do
    business_users = BusinessUsers.list_business_users()
    render(conn, "index.json", business_users: business_users)
  end

  def create(conn, %{"business_user" => business_user_params}) do
    with {:ok, %BusinessUser{} = business_user} <- BusinessUsers.create_business_user(business_user_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.business_user_path(conn, :show, business_user))
      |> render("show.json", business_user: business_user)
    end
  end

  def show(conn, %{"id" => id}) do
    business_user = BusinessUsers.get_business_user!(id)
    render(conn, "show.json", business_user: business_user)
  end

  def update(conn, %{"id" => id, "business_user" => business_user_params}) do
    business_user = BusinessUsers.get_business_user!(id)

    with {:ok, %BusinessUser{} = business_user} <- BusinessUsers.update_business_user(business_user, business_user_params) do
      render(conn, "show.json", business_user: business_user)
    end
  end

  def delete(conn, %{"id" => id}) do
    business_user = BusinessUsers.get_business_user!(id)

    with {:ok, %BusinessUser{}} <- BusinessUsers.delete_business_user(business_user) do
      send_resp(conn, :no_content, "")
    end
  end
end
