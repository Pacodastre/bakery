defmodule BakeryWeb.AdminOrderReportController do
  use BakeryWeb, :controller

  alias Bakery.Orders
  alias Bakery.Trolleys

  action_fallback BakeryWeb.FallbackController
  plug :put_view, BakeryWeb.AdminOrderReportView

  def report(conn, _params) do
    orders = Orders.list_orders(
      [
        :delivery,
        :payment,
        :status,
        :customer,
        trolley: [items: [:product, :special_offer]]
      ]
    )

    rows = orders
    |> Enum.map(fn order ->
      [
        "#{order.customer.first_name} #{order.customer.last_name}",
        "#{Bakery.DatetimeFormat.format_date(order.delivery.delivery_date)}",
        Enum.map(order.trolley.items, fn item ->
          if not is_nil(item.product) do
            "#{item.product.name} x #{item.quantity} = #{item.unit_price * item.quantity}"
          else
            "#{item.special_offer.name} x #{item.quantity} = #{item.unit_price * item.quantity}"
          end
        end) |> Enum.join("\n"),
        "#{Trolleys.calculate_trolley_total(order.trolley)}",
        if order.is_delivery do "Yes" else "No" end,
        "#{order.trolley.delivery_price}",
        "#{order.payment.type}",
        "#{order.payment.transaction_code}",
      ]
    end)

    conn
    |> put_resp_content_type("text/xlsx")
    |> put_resp_header("content-disposition", "attachment; filename=report")
    |> render("report.xlsx", %{rows: rows})
  end
end
