defmodule BakeryWeb.AdminOrderController do
  use BakeryWeb, :controller

  alias Bakery.Orders
  alias Bakery.Orders.Order
  alias Bakery.Mailer
  alias Bakery.ConfirmationKeys
  alias Bakery.BookingSlots
  alias Bakery.Settings

  action_fallback BakeryWeb.FallbackController
  plug :put_view, BakeryWeb.AdminOrderView

  def index(conn, _params) do
    orders = Orders.list_orders()
    render(conn, "index.json", orders: orders)
  end

  def create(conn, %{"order" => order_params}) do
    with {:ok, %Order{} = order} <- Orders.create_order(order_params) do
      order = order
      |> Bakery.Repo.preload([:delivery, :payment, trolley: [items: [:product]]])

      confirmation_order = ConfirmationKeys.create_confirmation_key(%{order_id: order.id})
      Mailer.send_confirmation_email(order, confirmation_order)

      conn
      |> put_status(:created)
      |> render("show.json", order: order)
    end
  end

  def show(conn, %{"id" => id}) do
    order = Orders.get_order!(id)
    render(conn, "show.json", order: order)
  end

  def update(conn, %{"id" => id, "order" => order_params}) do
    order = Orders.get_order!(id)

    with {:ok, %Order{} = order} <- Orders.admin_update_order(order, order_params) do
      render(conn, "show.json", order: order)
    end
  end

  def confirm(conn, %{"id" => id, "confirmation_key" => confirmation_key}) do
    with {:ok, %Order{} = order} <- Orders.confirm_order(id, confirmation_key) do
      order = order
      |> Bakery.Repo.preload([:delivery, trolley: [items: [:product]]])

      baker_email = Settings.get_setting_by_name!("baker_email").value
      Mailer.send_confirmation_email_to_baker(order, baker_email)
      render(conn, "show.json", order: order)
    end
  end

  def approve(conn, %{"id" => id, "message" => message}) do
    order = Orders.get_order!(id)

    with {:ok, %Order{} = order} <- Orders.approve_order(order, message) do
      order = order
      |> Bakery.Repo.preload([:delivery, trolley: [items: [:product, special_offer: :products]]])

      {:ok, _booking_slot} = order.delivery.delivery_date
      |> BookingSlots.find_booking_slot()
      |> BookingSlots.update_booking_slot(%{order_id: order.id})

      Mailer.send_approved_order_email(order)
      render(conn, "show.json", order: order)
    end
  end

  def decline(conn, %{"id" => id, "message" => message}) do
    order = Orders.get_order!(id)

    with {:ok, %Order{} = order} <- Orders.decline_order(order, message) do
      Mailer.send_declined_order_email(order)
      render(conn, "show.json", order: order)
    end
  end

  def delete(conn, %{"id" => id}) do
    order = Orders.get_order!(id)

    with {:ok, %Order{}} <- Orders.delete_order(order) do
      send_resp(conn, :no_content, "")
    end
  end
end
