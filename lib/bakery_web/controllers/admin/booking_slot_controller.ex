defmodule BakeryWeb.AdminBookingSlotController do
  use BakeryWeb, :controller

  alias Bakery.BookingSlots
  alias Bakery.BookingSlots.BookingSlot

  action_fallback BakeryWeb.FallbackController

  def index(conn, %{"date" => date}) do
    booking_slots = BookingSlots.admin_list_booking_slots(date)
    render(conn, "index.json", booking_slots: booking_slots)
  end

  def update(conn, %{"id" => id, "booking_slot" => booking_slot_params}) do
    booking_slot = BookingSlots.get_booking_slot!(id)

    with {:ok, %BookingSlot{} = booking_slot} <- BookingSlots.update_booking_slot(booking_slot, booking_slot_params) do
      render(conn, "show.json", booking_slot: booking_slot)
    end
  end

  def delete(conn, %{"id" => id}) do
    booking_slot = BookingSlots.get_booking_slot!(id)

    with {:ok, %BookingSlot{}} <- BookingSlots.delete_booking_slot(booking_slot) do
      send_resp(conn, :no_content, "")
    end
  end

  def show(conn, %{"id" => id}) do
    booking_slot = BookingSlots.get_booking_slot!(id)
    render(conn, "show.json", booking_slot: booking_slot)
  end

  def create(conn, %{"booking_slot" => booking_slot_params}) do
    with {:ok, %BookingSlot{} = booking_slot} <- BookingSlots.create_booking_slot(booking_slot_params) do
      conn
      |> put_status(:created)
      |> render("show.json", booking_slot: booking_slot)
    end
  end
end
