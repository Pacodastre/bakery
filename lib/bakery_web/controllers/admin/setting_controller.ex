defmodule BakeryWeb.AdminSettingController do
  use BakeryWeb, :controller

  alias Bakery.Settings
  alias Bakery.Settings.Setting

  plug :put_view, BakeryWeb.AdminSettingView
  action_fallback BakeryWeb.FallbackController

  def index(conn, _params) do
    settings = Settings.list_settings()
    render(conn, "index.json", settings: settings)
  end

  def create(conn, %{"setting" => setting_params}) do
    with {:ok, %Setting{} = setting} <- Settings.create_setting(setting_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.setting_path(conn, :show, setting))
      |> render("show.json", setting: setting)
    end
  end

  def show(conn, %{"id" => id}) do
    setting = Settings.get_setting!(id)
    render(conn, "show.json", setting: setting)
  end

  def update(conn, %{"_json" => params}) do
    with {:ok, settings} <- Settings.update_settings(params) do
      render(conn, "index.json", settings: settings)
    end
  end

  def delete(conn, %{"id" => id}) do
    setting = Settings.get_setting!(id)

    with {:ok, %Setting{}} <- Settings.delete_setting(setting) do
      send_resp(conn, :no_content, "")
    end
  end
end
