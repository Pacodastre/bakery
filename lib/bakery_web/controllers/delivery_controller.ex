defmodule BakeryWeb.DeliveryController do
  use BakeryWeb, :controller

  alias Bakery.Deliveries
  alias Bakery.Deliveries.Delivery

  action_fallback BakeryWeb.FallbackController

  def index(conn, _params) do
    deliveries = Deliveries.list_deliveries()
    render(conn, "index.json", deliveries: deliveries)
  end

  def create(conn, %{"delivery" => delivery_params}) do
    with {:ok, %Delivery{} = delivery} <- Deliveries.create_delivery(delivery_params) do
      conn
      |> put_status(:created)
      |> render("show.json", delivery: delivery)
    end
  end

  def show(conn, %{"id" => id}) do
    delivery = Deliveries.get_delivery!(id)
    render(conn, "show.json", delivery: delivery)
  end

  def update(conn, %{"id" => id, "delivery" => delivery_params}) do
    delivery = Deliveries.get_delivery!(id)

    with {:ok, %Delivery{} = delivery} <- Deliveries.update_delivery(delivery, delivery_params) do
      render(conn, "show.json", delivery: delivery)
    end
  end

  def delete(conn, %{"id" => id}) do
    delivery = Deliveries.get_delivery!(id)

    with {:ok, %Delivery{}} <- Deliveries.delete_delivery(delivery) do
      send_resp(conn, :no_content, "")
    end
  end
end
