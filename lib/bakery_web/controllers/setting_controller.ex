defmodule BakeryWeb.SettingController do
  use BakeryWeb, :controller

  alias Bakery.Settings

  action_fallback BakeryWeb.FallbackController

  def index(conn, _params) do
    settings = Settings.list_settings()
    render(conn, "index.json", settings: settings)
  end
end
