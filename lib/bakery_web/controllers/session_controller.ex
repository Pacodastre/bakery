defmodule BakeryWeb.SessionController do
  use BakeryWeb, :controller
  alias Bakery.Users

  def login(conn, %{"session" => session_params}) do
    case Users.login(session_params) do
      {:error, _message} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render("error.json")
      {:ok, jwt} ->
        conn
        |> put_status(:created)
        |> render("show.json", jwt: jwt)
    end

  end
end
