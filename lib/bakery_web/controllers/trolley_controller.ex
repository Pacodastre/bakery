defmodule BakeryWeb.TrolleyController do
  use BakeryWeb, :controller

  alias Bakery.Trolleys
  alias Bakery.Trolleys.Trolley

  action_fallback BakeryWeb.FallbackController

  def index(conn, _params) do
    trolleys = Trolleys.list_trolleys()
    render(conn, "index.json", trolleys: trolleys)
  end

  def create(conn, %{"trolley" => trolley_params}) do
    with {:ok, %Trolley{} = trolley} <- Trolleys.create_trolley(trolley_params) do
      conn
      |> put_status(:created)
      |> render("show.json", trolley: trolley)
    end
  end

  def show(conn, %{"id" => id}) do
    trolley = Trolleys.get_trolley!(id)
    render(conn, "show.json", trolley: trolley)
  end

  def update(conn, %{"id" => id, "trolley" => trolley_params}) do
    trolley = Trolleys.get_trolley!(id)

    with {:ok, %Trolley{} = trolley} <- Trolleys.update_trolley(trolley, trolley_params) do
      render(conn, "show.json", trolley: trolley)
    end
  end

  def delete(conn, %{"id" => id}) do
    trolley = Trolleys.get_trolley!(id)

    with {:ok, %Trolley{}} <- Trolleys.delete_trolley(trolley) do
      send_resp(conn, :no_content, "")
    end
  end
end
