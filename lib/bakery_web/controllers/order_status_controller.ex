defmodule BakeryWeb.OrderStatusController do
  use BakeryWeb, :controller

  alias Bakery.OrderStatuses

  action_fallback BakeryWeb.FallbackController

  def index(conn, _params) do
    order_statuses = OrderStatuses.list_order_statuses()
    render(conn, "index.json", order_statuses: order_statuses)
  end
end
