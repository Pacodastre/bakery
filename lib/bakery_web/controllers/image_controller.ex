defmodule BakeryWeb.ImageController do
  use BakeryWeb, :controller

  alias Bakery.Images
  alias Bakery.Images.Image

  action_fallback BakeryWeb.FallbackController

  def index(conn, _params) do
    images = Images.list_images()
    render(conn, "index.json", images: images)
  end

  def create(conn,  %{"image" => image_file}) do
    with {:ok, %Image{} = image} <- Images.create_image(image_file) do
      conn
      |> put_status(:created)
      |> render("show.json", image: image)
    end
  end

  def download(conn, %{"filename" => filename}) do
    image = Images.get_image_by!(%{filename: filename})
    Plug.Conn.send_file(conn, 200, "#{Application.get_env(:bakery, :upload_path)}/#{filename}")
    render(conn, "show.json", image: image)
  end

  def update(conn, %{"id" => id, "image" => image_params}) do
    image = Images.get_image!(id)

    with {:ok, %Image{} = image} <- Images.update_image(image, image_params) do
      render(conn, "show.json", image: image)
    end
  end

  def delete(conn, %{"id" => id}) do
    image = Images.get_image!(id)

    with {:ok, %Image{}} <- Images.delete_image(image) do
      send_resp(conn, :no_content, "")
    end
  end
end
