defmodule BakeryWeb.SpecialOfferController do
  use BakeryWeb, :controller

  alias Bakery.SpecialOffers

  action_fallback BakeryWeb.FallbackController

  def index(conn, _params) do
    special_offers = SpecialOffers.list_special_offers()
    render(conn, "index.json", special_offers: special_offers)
  end

  def show(conn, %{"id" => id}) do
    special_offer = SpecialOffers.get_special_offer!(id)
    render(conn, "show.json", special_offer: special_offer)
  end
end
