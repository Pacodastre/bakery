# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :bakery,
  ecto_repos: [Bakery.Repo]

# Configures the endpoint
config :bakery, BakeryWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "EZ4rLffv6fGE4CsG1IFiouNsnfEnm+Q/cEbytgRPT71XjdejHAqk7aqAjmntfh9h",
  render_errors: [view: BakeryWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Bakery.PubSub,
  live_view: [signing_salt: "2KZv7XMw"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :bakery, Bakery.Guardian,
  issuer: "bakery",
  secret_key: "8WKJ5lZ6QiNH/nCKOZRug5Ac49RLPX7swT0sD1u4x3TOoUzQGoQzcl3dYc+VeVcn"

config :bakery,
  mailgun_domain: System.get_env("MAILGUN_DOMAIN"),
  mailgun_key: System.get_env("MAILGUN_KEY"),
  mailgun_from_email: System.get_env("MAILGUN_FROM_EMAIL"),
  upload_path: System.get_env("UPLOAD_PATH")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
